import { Component, OnInit } from "@angular/core";
import { CoursesService } from "../../services/courses.service";
import { ICourse } from "../../models/course";
import { Router } from "@angular/router"


@Component({
    selector: "app-courses",
    templateUrl: "./courses.component.html",
    styleUrls: ["./courses.component.css"]
})
export class CoursesComponent implements OnInit {

    private courses;
    private errorMsg;
    constructor(private coursesService: CoursesService, private router: Router) {
    }

    public ngOnInit(): void {
        this.coursesService.getCourses()
          .subscribe(courses => this.courses = courses,
                     error => this.errorMsg = error);
    }

    private showDetail(course: ICourse) {
      this.router.navigate(["/courses", course.id]);
    }

    private newCourse() {
        this.router.navigate(["/newCourse"]);
    }
}
