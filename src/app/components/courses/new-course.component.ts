import { Component } from "@angular/core";
import { CoursesService } from "../../services/courses.service";
import { ICourse } from "../../models/course";
import { BsModalService, BsModalRef } from "ngx-bootstrap";
import { Router } from "@angular/router";

@Component({
    selector: "app-new-course",
    templateUrl: "./new-course.component.html",
    styleUrls: ["./new-course.component.css"]
})
export class NewCourseComponent {
    private course: ICourse;
    private errorMsg: string;
    private modalRef: BsModalRef;

    constructor(private coursesService: CoursesService,
                private modalService: BsModalService,
                private router: Router) {
      this.course = {id:0, title:'', image: ''}
    }

    public handleInputFile(files: FileList){
      this.coursesService.uploadFile(files.item(0))
        .subscribe(response => this.course.image = response.url,
                   error => this.errorMsg = error);
    }

    public create(successTmpl, errorTmpl) {
      this.coursesService.newCourse(this.course)
      .subscribe(response => {
                  this.course = response;
                  this.modalRef = this.modalService.show(successTmpl);
                 },
                 error => {
                     this.errorMsg = error;
                     this.modalRef = this.modalService.show(errorTmpl);
                 });
    }

    public closeOnSuccess(modal){
      this.modalRef.hide()
      this.router.navigate(["/courses"])
    }

    public closeOnError(modal){
      this.modalRef.hide()
    }
}
