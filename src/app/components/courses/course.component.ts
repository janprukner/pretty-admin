import { Component, OnInit } from "@angular/core";
import { ICourse } from "../../models/course";
import { ActivatedRoute } from "@angular/router";
import { CoursesService } from "../../services/courses.service";

@Component({
    selector: "app-course",
    templateUrl: "./course.component.html",
    styleUrls: ["./course.component.css"]
})
export class CourseComponent implements OnInit {

    private course: ICourse;
    private errorMsg;
    constructor(private route: ActivatedRoute, private coursesService: CoursesService) {
    }

    public ngOnInit(): void {
      this.coursesService.getCourseById(this.route.snapshot.paramMap.get("id"))
        .subscribe(course => this.course = course,
                   error => this.errorMsg = error);
    }

}
