export class NotificationModel {
    public title: string;
    public description: string;
    public page: string;
    public test: boolean = false;
}
