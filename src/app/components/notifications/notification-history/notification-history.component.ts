import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { NotificationService } from "../../../services/notification.service";

@Component({
    selector: "app-notification-history",
    templateUrl: "./notification-history.component.html",
    styleUrls: ["./notification-history.component.css"]
})
export class NotificationHistoryComponent implements OnInit, OnChanges {
    @Input() public reload: boolean = false;
    public notifications: Array<any> = [];
    public loading: boolean;
    @Output() public duplicateNotification = new EventEmitter();
    public notificationToDelete: any;
    public errorMessage: string = "";
    public modalRef: BsModalRef;


    constructor(private notificationService: NotificationService, public modalService: BsModalService) {
    }

    public ngOnInit() {
        this.loadNotificationHistory();
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (this.reload) {
            this.loadNotificationHistory();
        }
    }

    public async loadNotificationHistory(): Promise<void> {
        this.loading = true;
        let response: any = await this.notificationService.getNotificationsHistory();
        this.loading = false;
        console.log(response);
        if (response.status == "ok") {
            this.notifications = response.data;
            this.notifications.sort((a, b) => {
               return a.timestamp == b.timestamp ? 0 : +(a.timestamp < b.timestamp) || -1;
            });
            console.log(this.notifications);
        } else {
            console.log(response.status);
            console.log("error", response);
        }
    }

    public repeat(notification: {}): void {
        this.duplicateNotification.emit(notification);
    }

    public async deleteConfirmed(template: any): Promise<void> {
        const id = this.notificationToDelete["id"];
        this.modalRef.hide();
        this.loading = true;
        let response: any = await this.notificationService.deleteNotification(id);
        this.loading = false;
        console.log(response);
        if (response.status == "ok") {
            let index: number = this.notifications.indexOf(this.notificationToDelete);
            this.notifications.splice(index, 1);
        } else {
            this.errorMessage = "Network error";
            this.modalRef = this.modalService.show(template);
        }
    }

    public delete(notification: {}, template: any): void {
        this.notificationToDelete = notification;
        this.modalRef = this.modalService.show(template);
    }

}
