import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewContestComponent } from './new-contest.component';

describe('NewContestComponent', () => {
  let component: NewContestComponent;
  let fixture: ComponentFixture<NewContestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewContestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewContestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
