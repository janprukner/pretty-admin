import { Component, OnInit, TemplateRef } from "@angular/core";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { ContestsService } from "../../services/contests.service";
import { Utils } from "../../Utils/utils";

@Component({
    selector: "app-contests",
    templateUrl: "./contests.component.html",
    styleUrls: ["./contests.component.css"]
})
export class ContestsComponent implements OnInit {

    public contests: Array<any> = [];
    public currentContest: any;
    public contestToDelete: any;
    public errorMessage: string = "";
    public modalRef: BsModalRef;
    public loading: boolean = true;
    public contest: any;
    public utils: Utils = new Utils();

    constructor(private contestsService: ContestsService, private storage: Storage, private router: Router, public modalService: BsModalService) {
    }

    public ngOnInit(): void {
        this.getContests();
    }

    public async getContests(): Promise<void> {
        this.loading = true;
        let response: any = await this.contestsService.getContests();
        this.loading = false;
        console.log(response);
        if (response.status == "ok") {
            this.contests = response.data;
            this.contests.sort((a, b) => {
                if (a.status === b.status) {
                    return b.created - a.created;
                }
                return a.status < b.status? 1 : -1;
            });
            console.log("server", this.contests);
        } else if (response.status == "unauthorized") {
            await this.storage.set("user", undefined);
            this.router.navigate(["login"]);
        } else {
            console.log(response.status);
            console.log("error", response);
        }
    }

    public modify(contest: any): void {
        this.router.navigate(["newContest"], {queryParams: {id: contest.id, isNew: 0}});
    }

    public newContest(): void {
        this.router.navigate(["newContest"], {queryParams: {id: 0, isNew: 1}});
    }

    public delete(contest: any, template: any): void {
        this.contestToDelete = contest;
        this.modalRef = this.modalService.show(template);
    }

    public async deleteConfirmed(template: any): Promise<void> {
        this.modalRef.hide();
        this.loading = true;
        let response: any = await this.contestsService.deleteContest(this.contestToDelete.id);
        this.loading = false;
        if (response.status == "ok") {
            let index: number = this.contests.indexOf(this.contestToDelete);
            this.contests.splice(index, 1);
        } else if (response.status == "unauthorized") {
            await this.storage.set("user", undefined);
            this.router.navigate(["login"]);
        } else {
            this.errorMessage = "Network error";
            this.modalRef = this.modalService.show(template);
        }
    }

    public close(): void {
        this.modalRef.hide();
        this.getContests();
    }

    public showResults(id: number) {
        this.router.navigate(["result", id]);
    }

    public async copy(contest: any) {
        const dayDiff: number = this.utils.differenceInDays(contest.created.getTime(), contest.finishOn.getTime());
        const created: Date = new Date();
        const finish: Date = this.utils.addDays(created, dayDiff);
        finish.setHours(contest.finishOn.getHours());
        finish.setMinutes(contest.finishOn.getMinutes());
        finish.setSeconds(contest.finishOn.getSeconds());
        const contestDup: any = {
            contestType: contest.contestType,
            created: Math.round(created.getTime() / 1000),
            finishOn: Math.round(finish.getTime() / 1000),
            status: contest.status,
            answers: contest.answers,
            photoAnswers: contest.photoAnswers,
            image: contest.image,
            question: `Kopie - ${contest.question}`
        };
        let response: any = await this.contestsService.saveContest(contestDup, false);
        // reload new contests
        this.getContests();
    }
}
