export class Utils {
    public mapType(type: string) {
        switch (type) {
            case "voting":
                return "Hlasování";
            case "rating":
                return "Rating";
            case "text":
                return "Text";
        }
    }

    public mapState(state: string) {
        switch (state) {
            case "running":
                return "Probíhá";
            case "paused":
                return "Pozastaveno";
            case "finished":
                return "Ukončeno";
        }
    }

    public differenceInDays(firstDate: any, secondDate: any) {
        return Math.round((secondDate - firstDate) / (1000 * 60 * 60 * 24));
    }

    public addDays(date: Date, days: number) {
        const result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    }

    public convertArrayOfObjectsToCSV(args) {
        let result, ctr, keys, columnDelimiter, lineDelimiter, data;

        data = args.data || null;
        if (data == null || !data.length) {
            return null;
        }

        columnDelimiter = args.columnDelimiter || ",";
        lineDelimiter = args.lineDelimiter || "\n";

        keys = Object.keys(data[0]);

        result = "";
        result += keys.join(columnDelimiter);
        result += lineDelimiter;

        data.forEach(item => {
            ctr = 0;
            keys.forEach(key => {
                if (ctr > 0) result += columnDelimiter;

                result += item[key];
                ctr++;
            });
            result += lineDelimiter;
        });

        return result;
    }
}
