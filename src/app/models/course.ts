export interface ICourse {
  id: number;
  title: string;
  image: string;
}
