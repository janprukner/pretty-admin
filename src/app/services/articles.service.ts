import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions, Response } from "@angular/http";
import { Storage } from "@ionic/storage";
import { ProgressHttp } from "angular-progress-http";
import { apiUrl } from "../app.module";

@Injectable()
export class ArticlesService {
    private options: RequestOptions;

    constructor(private http: Http, private progressHttp: ProgressHttp, private storage: Storage) {
        let headers: Headers = new Headers({
            "Content-Type": "application/json"
        });
        this.options = new RequestOptions({headers: headers});
    }

    public async getArticles(): Promise<any> {
        try {
            let response: any = await this.http.get(apiUrl + "/articles").map((res: Response) => res.json()).toPromise();
            let data: any;
            for (let i = 0; i < response.length; i++) {
                if (response[i].key === "o-nas") {
                    data = response[i];
                    console.log("odpověď:", response);
                    break;
                }
            }
            return {
                status: "ok",
                data: data
            };
        } catch (err) {
            console.log(err);

            return {
                status: "err"
            };

        }

    }

    public async saveArticle(article: any, modify: boolean = false): Promise<any> {
        console.log("article", article);
        try {
            let data: string = JSON.stringify(article);
            let response: any;
            response = await this.http.put(`${apiUrl}/article/${article.id}`, data, this.options).map((res: Response) => res.json()).toPromise();
            console.log("odpověď:", response);
            return {
                status: "ok",
                data: response
            };
        } catch (err) {
            console.log(err);
            if (err.status == 400) {
                return {
                    status: "notAll"
                };
            } else {
                return {
                    status: "err"
                };
            }
        }
    }

}
