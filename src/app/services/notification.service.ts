import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions, Response } from "@angular/http";
import { Storage } from "@ionic/storage";
import { apiUrl } from "../app.module";
import { NotificationModel } from "../components/notifications/notification-model";

@Injectable()
export class NotificationService {

    constructor(private http: Http, private storage: Storage) {
    }

    public async sendNotification(notification: NotificationModel): Promise<any> {
        console.log("notification", notification);
        let userAdmin: any = await this.storage.get("user");
        try {
            let data: string = JSON.stringify(notification);
            let response: any;
            let headers: Headers = new Headers({
                "Content-Type": "application/json"
            });
            let options: RequestOptions = new RequestOptions({headers: headers});            // response = await this.http.post("/api/notifications/send", data, optionsGet).map((res: Response) => res.json()).toPromise();
            response = await this.http.post(apiUrl + "/notifications/send", data, options).map((res: Response) => res.json()).toPromise();
            console.log("odpověď:", response);
            return {
                status: "ok",
                data: response
            };
        } catch (err) {
            console.log(err);
            if (err.status == 400) {
                return {
                    status: "notAll"
                };
            } else {
                return {
                    status: "err"
                };
            }
        }
    }

    public async getNotificationsHistory(): Promise<any> {
        try {
            let response: any;
            // response = await this.http.get("/api/notifications/history", data, optionsGet).map((res: Response) => res.json()).toPromise();
            response = await this.http.get(apiUrl + "/notifications").map((res: Response) => res.json()).toPromise();
            console.log("odpověď:", response);
            return {
                status: "ok",
                data: response
            };
        } catch (err) {
            console.log(err);
            if (err.status == 400) {
                return {
                    status: "notAll"
                };
            } else {
                return {
                    status: "err"
                };
            }
        }
    }

    public async deleteNotification(id: number) : Promise<any> {
        console.log("Delete ", id)
        try {
            let headers: Headers = new Headers({
                "Content-Type": "application/json"
            });
            let options: RequestOptions = new RequestOptions({headers: headers});
            let response: any = await this.http.delete(`${apiUrl}/notification/${id}`, options).toPromise();
            console.log("odpověď:", response);
            return {
                status: "ok",
                data: response
            };
        } catch (err) {
            console.log(err);
            if (err.status == 400) {
                return {
                    status: "notAll"
                };
            } else {
                return {
                    status: "err"
                };
            }
        }
    }

}
