import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions, Response } from "@angular/http";
import { Storage } from "@ionic/storage";
import { HttpRequest } from "@angular/common/http";
import { ProgressHttp } from "angular-progress-http";
import { apiUrl } from "../app.module";
import { Subject } from "rxjs/Subject";

@Injectable()
export class ContestsService {
    private options: RequestOptions;
    constructor(private http: Http, private progressHttp: ProgressHttp, private storage: Storage) {
        let headers: Headers = new Headers({
            "Content-Type": "application/json"
        });
        this.options = new RequestOptions({headers: headers});
    }

    public async getContests(): Promise<any> {
        try {
           let response: any = await this.http.get(apiUrl + "/contests").map((res: Response) => res.json()).toPromise();
           response.forEach((item) => {
               item.created = new Date(item.created * 1000);
               item.finishOn = new Date(item.finishOn * 1000);
               item.timestamp = new Date(item.timestamp * 1000);
           });
           return {
               status: "ok",
               data: response
           };
        } catch (err) {
           console.log(err);

           return {
               status: "err"
           };

        }
    }

    public async saveContest(contest: any, modify: boolean = false): Promise<any> {
        console.log("contest", contest);
        try {
            let data: string = JSON.stringify(contest);
            let response: any;
            if (!modify) {
                response = await this.http.post(apiUrl + "/contest", data, this.options).map((res: Response) => res.json()).toPromise();
                // response = await this.http.post("/api/contest", data).map((res: Response) => res.json()).toPromise();
            } else {
                // response = await this.http.put(`/api/contest/${contest.id}`, data).map((res: Response) => res.json()).toPromise();
                response = await this.http.put(`${apiUrl}/contest/${contest.id}`, data, this.options).map((res: Response) => res.json()).toPromise();
            }
            console.log("odpověď:", response);
            return {
                status: "ok",
                data: response
            };
        } catch (err) {
            console.log(err);
            if (err.status == 400) {
                return {
                    status: "notAll"
                };
            } else {
                return {
                    status: "err"
                };
            }
        }
    }

    public async deleteContest(id: number): Promise<any> {
        try {
            let data: any = {
                id: id,
                deleted: true
            };
            // let response: any = await this.http.delete(`/api/contest/${id}`).toPromise();
            let response: any = await this.http.delete(`${apiUrl}/contest/${id}`, this.options).toPromise();
            console.log("odpověď:", response);
            return {
                status: "ok",
                data: response
            };
        } catch (err) {
            console.log(err);
            if (err.status == 400) {
                return {
                    status: "notAll"
                };
            } else {
                return {
                    status: "err"
                };
            }
        }
    }

    public async contestDetail(id: any): Promise<any> {
      try {
        let response: any = await this.http.get(apiUrl + "/contest/" + id).map((res: Response) => res.json()).toPromise();
        return {
          status: "ok",
          data: response
        };
      } catch (err) {
        console.log(err);
        return {
          status: "err"
        };
      }
    }

    public async getResults(id: number) {
        try {
            let response: any = await this.http.get(`${apiUrl}/contest/${id}/results`).map((res: Response) => res.json()).toPromise();
            response.forEach((item) => {item.timestamp = new Date(item.timestamp * 1000);});
            return {
                status: "ok",
                data: response
            };
        } catch (err) {
            console.log(err);

            return {
                status: "err",
                data: undefined
            };

        }
    }

    public async uploadFile(file: File, type?: number): Promise<any> {
        try {
            let formData: FormData = new FormData();
            formData.append("file", file, file.name);
            const req: any = new HttpRequest("POST", apiUrl + "/file/upload", formData, {
                // const req: any = new HttpRequest("POST", "/api/file/upload", formData, {
                reportProgress: true,
            });
            let response: any = await this.progressHttp
                .withUploadProgressListener((progress: any) => {
                    //if (type) {
                    //    this.sendSubject(type, progress.percentage);
                    //}
                    // console.log(`Uploading ${progress.percentage}%`);
                })
                // .post("/api/file/upload", formData, optionsGet)
                .post(apiUrl + "/file/upload", formData)
                .map((res: Response) => res.json()).toPromise();
            console.log("odpověď:", response);
            return {
                status: "ok",
                data: response
            };
        } catch (err) {
            console.log(err);
            return {
                status: "err"
            };

        }
    }

}
