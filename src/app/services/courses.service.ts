import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { ICourse } from "../models/course";
import {Observable } from "rxjs/Observable"
import "rxjs/add/operator/catch"
import "rxjs/add/observable/throw"
import { apiUrl } from "../app.module";

@Injectable()
export class CoursesService {
    constructor(private http: HttpClient ) {
    }

    public getCourses(): Observable<ICourse[]> {
      return this.http.get<ICourse[]>(apiUrl + "/courses")
                      .catch(err => Observable.throw(err.message || "Server Error"));
    }

    public getCourseById(id: string): Observable<ICourse> {
      return this.http.get<ICourse>(apiUrl + "/courses/" + id)
                      .catch(err => Observable.throw(err.message || "Server Error"));
    }

    public uploadFile(fileToUpload: File): Observable<FileUploadResponse> {
      const endpoint = apiUrl + '/file/upload';
      const formData: FormData = new FormData();
      formData.append('file', fileToUpload, fileToUpload.name);
      return this.http
        .post(endpoint, formData)
        .catch(err => Observable.throw(err.message || "Server Error"));
    }

    public newCourse(course: ICourse): Observable<ICourse> {
      let c = {
        'title': course.title,
        'image': course.image
      };
      return this.http
        .post(apiUrl + '/course', JSON.stringify(c), {headers: {"Content-Type":"application/json"}})
        .catch(err => Observable.throw(err.message || "Server Error"));
    }
}
